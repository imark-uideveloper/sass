(function($) {
  'use strict';

  // Testimonials
  // var testimonials = $('#testimonials');
  // if (testimonials.length > 0) {
  //   testimonials.owlCarousel({
  //     loop: true,
  //     margin: 0,
  //     items: 1,
  //     nav: false,
  //     dots: true
  //   })
  // }


  //Avoid pinch zoom on iOS
  document.addEventListener('touchmove', function(event) {
    if (event.scale !== 1) {
      event.preventDefault();
    }
  }, false);
})(jQuery)
